﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class SetPersonnality : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {

        ShopperBehavior shopperBehavior = self.GetComponent<ShopperBehavior>() as ShopperBehavior;
        if (shopperBehavior != null)
        {
            blackboard.GetFloatVar("Strenght").Value = shopperBehavior.strength;
            //blackboard.GetFloatVar("Bravery").Value = shopperBehavior.bravery;
            //blackboard.GetFloatVar("Excitation").Value = shopperBehavior.excitation;
            blackboard.GetFloatVar("Awareness").Value = shopperBehavior.awareness;
            blackboard.GetFloatVar("Eager").Value = shopperBehavior.eager;
            //blackboard.GetFloatVar("Independance").Value = shopperBehavior.independence;
            //blackboard.GetFloatVar("Morality").Value = shopperBehavior.morality;

            status = Status.Success;
        }
        else
            status = Status.Error;

    }
}
