﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
using System.Collections.Generic;
public class SelectNewShop : ActionNode
{

    public GameObjectVar gameObject;

    // Called when the node will be ticked
    public override void OnTick()
    {

        ShopperIdle idleEntity = self.GetComponent<ShopperIdle>() as ShopperIdle;
        GameObject unityGameObj = (GameObject)gameObject;
        Shop shopDoor = unityGameObj.GetComponent<Shop>() as Shop;


        if (idleEntity.currentTargetShopNode == shopDoor.entryNode)
        {
            idleEntity.shopNodeList.RemoveAt(0);
            idleEntity.shopPositionList.RemoveAt(0);

            if (idleEntity.shopNodeList.Count > 0)
            {
                // Going to a new shop
                idleEntity.currentTargetShopNode = idleEntity.shopNodeList[0];
                idleEntity.targetNode = null;
                idleEntity.nodeListToCurrentTargetShop = AStar.GetPath(idleEntity.currentNode, idleEntity.currentTargetShopNode);
            }
            else
            {
                // Leaving the mall
                idleEntity.currentTargetShopNode = null;
                idleEntity.nodeListToCurrentTargetShop.Clear();
                idleEntity.wantToExit = true;
            }
        }
        status = Status.Success;
    }
}
