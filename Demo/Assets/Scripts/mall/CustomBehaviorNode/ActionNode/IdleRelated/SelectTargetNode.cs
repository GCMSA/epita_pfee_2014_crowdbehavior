﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
using System.Collections.Generic;
public class SelectTargetNode : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {

        ShopperIdle idleEntity = self.GetComponent<ShopperIdle>() as ShopperIdle;
        ShopperNavigation navigationEntity = self.GetComponent<ShopperNavigation>() as ShopperNavigation;

        navigationEntity.disableRVO = false;

        if (idleEntity.wantToExit == true)
        {
            // If not arrived at exit
            if (idleEntity.currentNode.nextNodeToExit)
                idleEntity.targetNode = idleEntity.currentNode.nextNodeToExit;

        }
        // Not arrived to current goal
        else if (idleEntity.nodeListToCurrentTargetShop.Count > 1)
        {
            //idleEntity.nodeListToCurrentTargetShop.RemoveAt(0);
            idleEntity.targetNode = idleEntity.nodeListToCurrentTargetShop[0];
        }

        navigationEntity.TargetPosition = idleEntity.targetNode.transform.position;
    }
}
