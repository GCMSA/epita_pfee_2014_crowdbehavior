﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
using System.Collections.Generic;
public class PickUpShops : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {

        ShopperIdle idleEntity = self.GetComponent<ShopperIdle>() as ShopperIdle;

        idleEntity.shopNodeList.Clear();
        idleEntity.shopPositionList.Clear();

        // Took 3 to 7 shop to visit in the mall
        int numberOfShopToGo = Random.Range(3, 7);

        // Copy the list of all shops in the mall
        List<GameObject> shopList = new List<GameObject>(GlobalEntities.doorShopList);

        for (int i = 0; i < numberOfShopToGo; i++)
        {
            // Pick a random shop in the mall
            int shopIdx = Random.Range(0, shopList.Count);
            GameObject currentShop = shopList[shopIdx];

            // When a shop is choosen by a shopper it cannot be choosen again.
            shopList.RemoveAt(shopIdx);

            // Find the door of the selected shop
            Shop doorShop = currentShop.GetComponent(typeof(Shop)) as Shop;

            // Take its location and its corresponding node then adding to goalLists
            idleEntity.shopNodeList.Add(doorShop.entryNode);
            idleEntity.shopPositionList.Add(doorShop.transform.position);
        }

        idleEntity.currentTargetShopNode = idleEntity.shopNodeList[0];

        // Compute AStar for the new shop
        idleEntity.nodeListToCurrentTargetShop = AStar.GetPath(idleEntity.currentNode, idleEntity.currentTargetShopNode);

        status = Status.Success;
    }
}
