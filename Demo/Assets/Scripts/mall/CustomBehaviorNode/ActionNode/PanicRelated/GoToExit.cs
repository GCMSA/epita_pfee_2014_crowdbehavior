﻿
using UnityEngine;
using System.Collections;
using BehaviourMachine;
public class GoToExit : ActionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>() as ShopperGlobal;

        NavNode nodeToExit = shopperEntity.navigationEntity.currentNode.nextNodeToExit;

        if (nodeToExit)
        {
            shopperEntity.navigationEntity.TargetPosition = nodeToExit.cachedTransform.position;
            //shopperEntity.navigationEntity.speed *= 1.5f;
            //float distanceToTargetNode = Vector3.Distance(shopperEntity.transform.position, nodeToExit.cachedTransform.position);

            //Debug.Log("Distance: " + distanceToTargetShopper);

            // Disable RVO when getting close in order to avoid RVO preventing collision
            //if (distanceToTargetNode < 1.5) ;

            //if (distanceToTargetShopper > 0.6)
            //{
            //    Vector3 pos = shopperEntity.transform.position;
            shopperEntity.navigationEntity.disableRVO = true;
            shopperEntity.navigationEntity.Move();

            status = Status.Running;
            return;
            //}

            //shopperEntity.actionBehaviorEntity.shopperToSteal = null;
            //float duration = Random.Range(3, 7);
            //GlobalEntities.areaHandler.createArea(self.transform.position, Area.AreaType.STEAL, duration, 10, shopperEntity);
        }
        //else
        //{
        //    shopperEntity.navigationEntity.Move();

        //    status = Status.Running;
        //    return;
        //}



        GameObject.Destroy(shopperEntity.gameObject);
        status = Status.Success;
    }
}
