﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;
using System.Collections.Generic;
public class IsArrivedAtTargetNode : ConditionNode
{

    // Called when the node will be ticked
    public override void OnTick()
    {
        status = Status.Failure;

        ShopperGlobal shopperEntity = self.GetComponent<ShopperGlobal>() as ShopperGlobal;

        if (shopperEntity.idleBehaviorEntity.targetNode != null && shopperEntity.idleBehaviorEntity.currentTargetShopNode != shopperEntity.idleBehaviorEntity.targetNode)
        {
            if (Vector3.Distance(shopperEntity.transform.position, shopperEntity.idleBehaviorEntity.targetNode.cachedTransform.position) < shopperEntity.idleBehaviorEntity.targetNode.Radius)
                status = Status.Success;
        }
        

    }
}
