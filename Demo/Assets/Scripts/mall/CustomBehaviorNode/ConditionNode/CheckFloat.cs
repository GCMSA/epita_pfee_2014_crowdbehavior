﻿using UnityEngine;
using System.Collections;
using BehaviourMachine;

public class CheckFloat : ConditionNode
{

		public enum Comparison
		{
				SUPERIOR,
				INFERIOR
		}

		public FloatVar floatToCheck;
		public FloatVar toPower;
		public FloatVar minRandom;
		public FloatVar maxRandom;
		public Comparison comparaison;


		// Called when the node will be ticked
		public override void OnTick ()
		{
				float rand = Random.Range (minRandom, maxRandom);
				//Debug.Log("Random:" + rand);
				if (comparaison == Comparison.INFERIOR) {
						if (floatToCheck < rand)
								status = Status.Success;
						else
								status = Status.Failure;
				} else if (comparaison == Comparison.SUPERIOR) {
						if (floatToCheck > rand)
								status = Status.Success;
						else
								status = Status.Failure;
				}
				//				if (comparaison == Comparison.INFERIOR) {
//						if (Mathf.Pow (floatToCheck, toPower) < rand)
//								status = Status.Success;
//						else
//								status = Status.Failure;
//				} else if (comparaison == Comparison.SUPERIOR) {
//						if (Mathf.Pow (floatToCheck, toPower) > rand)
//								status = Status.Success;
//						else
//								status = Status.Failure;
//				}
		}
}
