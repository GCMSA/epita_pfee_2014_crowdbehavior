﻿using UnityEngine;
using System.Collections;

public class SpawnNode : MonoBehaviour
{
		
		public float weight = 1f;
		public float YSpawnPos = 2f;
		public Transform SpawnParent;

		// Use this for initialization
		void Start ()
		{
		}

		void OnTriggerEnter (Collider other)
		{
				ShopperGlobal agent = other.GetComponent<ShopperGlobal> ();
				if (agent != null && agent.navigationEntity != null && agent.idleBehaviorEntity.wantToExit) {
						Object.Destroy (agent.gameObject, 2);
//						GlobalEntities.shopperGenerator.generateShopper ();

				}
		}

		// Update is called once per frame
		void Update ()
		{
	
		}

		void OnDrawGizmos ()
		{
				Gizmos.color = Color.blue;
				Gizmos.DrawSphere (transform.position, 1.5f);
		}
}
