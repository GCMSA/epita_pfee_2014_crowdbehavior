﻿using UnityEngine;
using System.Collections;

public class ShopperBehavior : MonoBehaviour
{

		// float speed; // slow <-> fast // Speed is in ShopperNav Class
		public float strength = 0.5f; // weak <-> strong
		//public float bravery = 0.5f; // fearful <-> brave
		//public float excitation = 0.5f; // calm <-> excited
		public float awareness = 0.5f; // unaware <-> aware
		public float eager = 0.5f; // selfless <-> greedy
		//public float independence = 0.5f; // follower <-> leader
		//public float morality = 0.5f; // bad <-> good
		public State state = State.IDLE;

		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{

		}

		public enum State
		{
				IDLE = 0,
				ALERT,
				STEAL,
				ACTION,
				DELEGATION,
				PANIC,
                FIGHT_GOOD,
                FIGHT_BAD
		}
}
