﻿using UnityEngine;
using System.Collections;

public class ShopperGenerator : MonoBehaviour
{
    [HideInInspector]
    public int CurrentShopperCount = 0;
    public int NumberOfShoppers = 200;
    public GameObject[] Templates;
    public float[] Weights;


    // Generate NumberOfShoppers Shopper
    void Start()
    {
        //				for (int i = 0; i < NumberOfShoppers; i++) {
        //						generateShopper ();
        //				}
        //				GeneratedShoppers = NumberOfShoppers;
    }

    // Update is called once per frame
    void Update()
    {
        if (CurrentShopperCount < NumberOfShoppers)
        {
            generateShopper();
            ++CurrentShopperCount;
        }
    }

    public void generateShopper()
    {
        // Spawn node
        SpawnNode sn = GetRandomSpawnNode();

        // Choose a random location within the spawnRadius
        var boxCollider = sn.collider as BoxCollider;
        float randXPos = Random.Range(sn.transform.position.x - (boxCollider.size.x / 2), sn.transform.position.x + (boxCollider.size.x / 2));
        float randZPos = Random.Range(sn.transform.position.z - (boxCollider.size.z / 2), sn.transform.position.z + (boxCollider.size.z / 2));
        Vector3 randPos = new Vector3(randXPos, sn.YSpawnPos, randZPos);

        // Instantiate and make the enemy a child of this object
        GameObject o = (GameObject)MonoBehaviour.Instantiate(getRandomTemplate(), randPos, sn.transform.rotation);

        // No string comparison for this!
        o.transform.parent = sn.SpawnParent;

        if (o.CompareTag("Agent"))
        { // Prefer GetComponent<ShopperGlobal>() != null
            ShopperTemplate template = o.GetComponent(typeof(ShopperTemplate)) as ShopperTemplate;

            // Shopper Nav
            ShopperNavigation agentNav = o.GetComponent(typeof(ShopperNavigation)) as ShopperNavigation;
            agentNav.speed = template.SpeedDistribution.Evaluate();
            agentNav.maxSpeed = agentNav.speed * 2;

			// Change shape
			float max = 3f;
			float min = 0f;
			Transform cylinder = o.transform.FindChild("Animated").FindChild("Cylinder");
			Vector3 vec = cylinder.localScale;
            vec.x = vec.z = (((agentNav.speed - min) * (0.8f - 0.4f)) / (max - min)) + 0.4f;
			vec.y = (((agentNav.speed - min) * (1.2f - 0.6f)) / (max - min)) + 0.6f;
			cylinder.localScale = vec;
			vec = o.transform.localScale;
			vec.y = cylinder.localScale.y;
			o.transform.localScale = vec;

            // Shopper Idle
            ShopperIdle idleAgent = o.GetComponent(typeof(ShopperIdle)) as ShopperIdle;

            // Shopper Behavior
            ShopperBehavior agentBehavior = o.GetComponent(typeof(ShopperBehavior)) as ShopperBehavior;
            agentBehavior.strength = template.StrengthDistribution.Evaluate();
            //agentBehavior.bravery = template.BraveryDistribution.Evaluate();
            //agentBehavior.excitation = template.ExcitationDistribution.Evaluate();
            agentBehavior.awareness = template.AwarenessDistribution.Evaluate();
            agentBehavior.eager = template.EagerDistribution.Evaluate();
            //agentBehavior.independence = template.IndependenceDistribution.Evaluate();
            //agentBehavior.morality = template.MoralityDistribution.Evaluate();

            foreach (Transform child in o.transform)
            {
                foreach (Transform childChild in child)
                    childChild.renderer.material = GlobalEntities.stateMaterials[(int)agentBehavior.state];
            }

            NavNode currentNode = sn.GetComponent(typeof(NavNode)) as NavNode;
            agentNav.currentNode = currentNode;
            idleAgent.currentNode = currentNode;

        }
    }

    private SpawnNode GetRandomSpawnNode()
    {
        SpawnNode[] SpawnArray = new SpawnNode[GlobalEntities.spawnNodeList.Length];
        for (int i = 0; i < SpawnArray.Length; ++i)
            SpawnArray[i] = GlobalEntities.spawnNodeList[i].GetComponent(typeof(SpawnNode)) as SpawnNode;

        float[] cumulativeWeights = new float[SpawnArray.Length];
        cumulativeWeights[0] = SpawnArray[0].weight;

        for (int i = 1; i < SpawnArray.Length; ++i)
            cumulativeWeights[i] = cumulativeWeights[i - 1] + SpawnArray[i].weight;

        float r = Random.Range(0, cumulativeWeights[SpawnArray.Length - 1]);

        for (int i = 0; i < SpawnArray.Length; ++i)
            if (r <= cumulativeWeights[i])
                return SpawnArray[i];

        return null;
    }

    private GameObject getRandomTemplate()
    {
        float[] cumulativeWeights = new float[Templates.Length];
        cumulativeWeights[0] = Weights[0];
        for (int i = 1; i < Templates.Length; ++i)
        {
            cumulativeWeights[i] = cumulativeWeights[i - 1] + Weights[i];
        }

        float r = Random.Range(0, cumulativeWeights[Templates.Length - 1]);

        for (int i = 0; i < Templates.Length; ++i)
            if (r <= cumulativeWeights[i])
                return Templates[i];

        return null;
    }

    public enum Distribution
    {
        UNIFORM,
        NORMAL,
        LOW,
        HIGH,
        EXTREME
    }



}

